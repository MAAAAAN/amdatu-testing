/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.testing.mongo;

import static org.amdatu.testing.configurator.TestUtils.getBundleContext;
import static org.amdatu.testing.configurator.TestUtils.getFactoryConfiguration;

import java.io.IOException;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.amdatu.mongo.MongoDBService;
import org.amdatu.testing.configurator.ConfigurationStep;
import org.amdatu.testing.configurator.TestConfigurationException;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.Filter;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.Configuration;
import org.osgi.util.tracker.ServiceTracker;
import org.osgi.util.tracker.ServiceTrackerCustomizer;

/**
 * A configuration step that sets up a temporary mongo database and drops it on cleanup.
 */
public class MongoConfigurationStep implements ConfigurationStep {

    private String m_dbName;
    private String m_host;
    private String m_username;
    private String m_password;
    private String m_port;

    private Configuration m_configuration;
    private String m_configurationPid;
    private int m_duration;
    private TimeUnit m_unit;

    public MongoConfigurationStep() {
        m_duration = 5;
        m_unit = TimeUnit.SECONDS;
    }

    @Override
    public void apply(Object testCase) {
        m_dbName = "test-" + System.currentTimeMillis() + "-" + testCase.getClass().getSimpleName();

        m_configuration = getFactoryConfiguration("org.amdatu.mongo");
        m_configurationPid = m_configuration.getPid();
        try {
            Dictionary<String, Object> props = createConfigurationDictionary();
            m_configuration.update(props);
        } catch (IOException e) {
            throw new TestConfigurationException("Could not configure MongoDB: update of configuration failed!", e);
        }
    }

    @Override
    public void cleanUp(Object testCase) {
        if (m_configuration == null) {
            // Nothing to do: apply was never called!
            return;
        }

        BundleContext context = getBundleContext(testCase.getClass());

        CountDownLatch removedLatch = new CountDownLatch(1);

        ServiceTracker<MongoDBService, MongoDBService> tracker = new ServiceTracker<>(context, createFilter(), new ServiceTrackerCustomizer<MongoDBService, MongoDBService>() {
            @Override
            public MongoDBService addingService(ServiceReference<MongoDBService> reference) {
                MongoDBService srv = context.getService(reference);
                if (srv != null) {
                    try {
                        // Drop the database...
                        srv.getDB().dropDatabase();
                    } catch (com.mongodb.MongoException e) {
                        e.printStackTrace();
                    }
                }
                return srv;
            }

            @Override
            public void modifiedService(ServiceReference<MongoDBService> reference, MongoDBService service) {
                // Ignore, not relevant to us...
            }

            @Override
            public void removedService(ServiceReference<MongoDBService> reference, MongoDBService service) {
                removedLatch.countDown();
            }
        });
        // Start listening...
        tracker.open();

        try {
            // This is asynchronous, so we cannot be sure the MongoDBService instance is gone and we have to wait for the service to go away...
            m_configuration.delete();

            // Only fail if the database was there, but we did not see it going away within the set time boundaries...
            if (!removedLatch.await(m_duration, m_unit)) {
                throw new TestConfigurationException("Could not remove MongoDBService with pid " + m_configurationPid
                    + " within " + m_duration + " " + m_unit.name().toLowerCase());
            }
        } catch (IOException | InterruptedException e) {
            throw new TestConfigurationException("Problem doing cleanup for MongoDBService with pid " + m_configurationPid, e);
        } finally {
            tracker.close();
            // We're done with this configuration...
            m_configuration = null;
        }
    }

    /**
     * The time to wait for during the cleanup before the mongoDB service should go away.
     *
     * @param duration the duration to use.
     * @param unit the time unit to use, cannot be <code>null</code>.
     * @return this configuration step, never <code>null</code>.
     */
    public MongoConfigurationStep setTimeout(int duration, TimeUnit unit) {
        m_duration = duration;
        m_unit = unit;
        return this;
    }

    /**
     * Sets the host that MongoDBService should connect to.
     *
     * @param host the host to connect to.
     * @return this configuration step, never <code>null</code>.
     */
    public MongoConfigurationStep setHost(String host) {
        m_host = host;
        return this;
    }

    /**
     * Sets the port that MongoDBService should connect to.
     *
     * @param port the port to connect to.
     * @return this configuration step, never <code>null</code>.
     */
    public MongoConfigurationStep setPort(int port) {
        m_port = "" + port;
        return this;
    }

    /**
     * Sets the username that MongoDBService should use to connect.
     *
     * @param username the username to connect withpassword.
     * @return this configuration step, never <code>null</code>.
     */
    public MongoConfigurationStep setUsername(String username) {
        m_username = username;
        return this;
    }

    /**
     * Sets the password that MongoDBService should use to connect.
     *
     * @param password the password to connect with.
     * @return this configuration step, never <code>null</code>.
     */
    public MongoConfigurationStep setPassword(String password) {
        m_password = password;
        return this;
    }

    private Filter createFilter() {
        try {
            return FrameworkUtil.createFilter(String.format("(&(%s=%s)(dbName=%s))", Constants.OBJECTCLASS, MongoDBService.class.getName(), m_dbName));
        } catch (InvalidSyntaxException e) {
            throw new TestConfigurationException("Failed to create filter!", e);
        }
    }

    private Dictionary<String, Object> createConfigurationDictionary(){
        Dictionary<String, Object> props = new Hashtable<>();
        props.put("dbName", m_dbName);
        setIfNotEmpty(props, "host", m_host);
        setIfNotEmpty(props, "port", m_port);
        setIfNotEmpty(props, "username", m_username);
        setIfNotEmpty(props, "password", m_password);
        return props;
    }

    private static void setIfNotEmpty(Dictionary<String, Object> props, String key, String value){
        if(!isEmpty(value)){
            props.put(key, value);
        }
    }

    private static boolean isEmpty(String s){
        return s == null || s.equals("");
    }
}
