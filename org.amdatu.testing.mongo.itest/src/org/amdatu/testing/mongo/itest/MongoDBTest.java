/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.testing.mongo.itest;

import static org.amdatu.testing.configurator.TestConfigurator.cleanUp;
import static org.amdatu.testing.configurator.TestConfigurator.configure;
import static org.amdatu.testing.mongo.OSGiMongoTestConfigurator.configureMongoDb;
import static org.junit.Assert.assertTrue;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.amdatu.mongo.MongoDBService;
import org.junit.Test;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;

public class MongoDBTest {

    /**
     * Tests that the configuration of MongoDB works as expected and that the database that is created goes away when cleaning up.
     * <p>
     * This should test AMDATUTEST-5 and AMDATUTEST-24?!
     * </p>
     */
    @Test
    public void testConfigureMongoDB() throws Exception {
        CountDownLatch addedLatch = new CountDownLatch(1);
        CountDownLatch removedLatch = new CountDownLatch(1);

        ServiceListener listener = event -> {
            if (event.getType() == ServiceEvent.REGISTERED) {
                addedLatch.countDown();
            } else if (event.getType() == ServiceEvent.UNREGISTERING) {
                removedLatch.countDown();
            }
        };

        BundleContext bc = FrameworkUtil.getBundle(getClass()).getBundleContext();
        bc.addServiceListener(listener, "(objectClass=" + MongoDBService.class.getName() + ")");

        configure(this)
            .add(configureMongoDb().setTimeout(10, TimeUnit.SECONDS))
            .apply();

        try {
            assertTrue("MongoDBService should be registered!", addedLatch.await(5, TimeUnit.SECONDS));
        } finally {
            cleanUp(this);
        }

        assertTrue("MongoDBService should be unregistered!", removedLatch.await(5, TimeUnit.SECONDS));
    }
}