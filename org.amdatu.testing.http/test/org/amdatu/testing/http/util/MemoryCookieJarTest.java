/*
 * Copyright (c) 2010-2016 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.testing.http.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.net.URI;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Iterator;
import java.util.Set;

import org.amdatu.testing.http.util.MemoryCookieJar;
import org.amdatu.testing.http.util.MemoryCookieJar.Cookie;
import org.junit.Test;

/**
 * Test cases for {@link MemoryCookieJar}.
 */
public class MemoryCookieJarTest {

    @Test
    public void testMatchCookieDefaultPathOk() throws Exception {
        String[][] testCases = { { "", "/" }, //
            { "/", "/" }, //
            { "/file", "/" }, //
            { "/dir/file", "/dir" }, //
            { "/dir/", "/dir" }, //
            { "dir/", "/" }, //
            { "dir", "/" }, //
        };

        for (String[] testCase : testCases) {
            String input = testCase[0];
            String path = testCase[1];

            assertEquals(input, path, MemoryCookieJar.defaultPath(input));
        }
    }

    @Test
    public void testMatchCookieDomainFail() throws Exception {
        String[][] testCases = { { "no.ca", "yes.ca" }, //
            { "example.com", "www.example.com" }, //
            { "wwwexample.com", "example.com" }, //
            { "example.com", "example.com." }, //
            { "192.168.0.1", "168.0.1" }, //
        };

        for (String[] testCase : testCases) {
            String input = testCase[0];
            String domain = testCase[1];

            Cookie c = Cookie.parse("key=value; domain=" + domain);
            assertNotNull(c);
            assertFalse(input, c.matchesDomain(input));
        }
    }

    @Test
    public void testMatchCookieDomainOk() throws Exception {
        String[][] testCases = { { "example.com", "example.com" }, //
            { "eXaMpLe.cOm", "ExAmPlE.CoM" }, //
            { "www.example.com", "example.com" }, //
            { "www.subdom.example.com", "example.com" }, //
            { "www.subdom.example.com", "subdom.example.com" }, //
        };

        for (String[] testCase : testCases) {
            String input = testCase[0];
            String domain = testCase[1];

            Cookie c = Cookie.parse("key=value; domain=" + domain);
            assertNotNull(c);
            assertTrue(input, c.matchesDomain(input));
        }
    }

    @Test
    public void testMatchCookiePathFail() throws Exception {
        String[][] testCases = { { "/", "/dir" }, //
            { "/directory", "/dir" }, //
        };

        for (String[] testCase : testCases) {
            String input = testCase[0];
            String path = testCase[1];

            Cookie c = Cookie.parse("key=value; path=" + path);
            assertNotNull(c);
            assertFalse(input, c.matchesDomain(input));
        }
    }

    @Test
    public void testMatchCookiePathOk() throws Exception {
        String[][] testCases = { { "/", "/" }, //
            { "/dir", "/" }, //
            { "/dir/", "/dir/" }, //
            { "/dir/", "/dir" }, //
            { "/dir/file", "/dir/" }, //
            { "/dir/file", "/dir" }, //
            { "/dir/file", "/dir" }, //
        };

        for (String[] testCase : testCases) {
            String input = testCase[0];
            String path = testCase[1];

            Cookie c = Cookie.parse("key=value; path=" + path);
            assertNotNull(c);
            assertTrue(input, c.matchesPath(input));
        }
    }

    @Test
    public void testParseCookiesFormsOk() throws Exception {
        assertNotNull(Cookie.parse("key=value"));
        assertNotNull(Cookie.parse("key=value;"));
        assertNotNull(Cookie.parse(" key = value ; attr1=value1"));
        assertNull(Cookie.parse("=value"));
        assertNull(Cookie.parse(";"));
    }

    @Test
    public void testParseCookieStringOk() throws Exception {
        Cookie cookie = Cookie.parse("key=value; Expires=Wed, 09 Jun 2021 10:18:14 GMT");
        assertNotNull(cookie);
    }

    @Test
    public void testParseCookiesWithInvalidExpirationDateOk() throws Exception {
        String[] invalidDatePatterns = { "99 Jix 3038 48:86:72 ZMT", //
            "01 Jan 1600 00:00:00 GMT", // before 1601
        };

        for (String pattern : invalidDatePatterns) {
            Cookie cookie = Cookie.parse("key=value; expires=" + pattern);
            assertNull(pattern, cookie.getExpirationTime());
        }
    }

    @Test
    public void testGetHostOnlyCookiesOk() throws Exception {
        MemoryCookieJar jar = new MemoryCookieJar();
        jar.put(URI.create("http://amdatu.org/foo"), "key=value; path=/");
        jar.put(URI.create("http://amdatu.org/foo"), "key=value; path=/foo");

        Set<Cookie> cookies;
        Iterator<Cookie> iter;
        Cookie c1, c2;

        cookies = jar.getCookies(URI.create("http://amdatu.org/"));
        assertNotNull(cookies);
        assertEquals(1, cookies.size());

        iter = cookies.iterator();
        c1 = iter.next();
        assertEquals("/", c1.getPath());
        assertEquals("amdatu.org", c1.getDomain());

        cookies = jar.getCookies(URI.create("http://amdatu.org/foo"));
        assertNotNull(cookies);
        assertEquals(2, cookies.size());

        iter = cookies.iterator();
        c1 = iter.next();
        assertEquals("/foo", c1.getPath());
        assertEquals("amdatu.org", c1.getDomain());

        c2 = iter.next();
        assertEquals("/", c2.getPath());
        assertEquals("amdatu.org", c2.getDomain());
    }

    @Test
    public void testGetDomainCookiesOk() throws Exception {
        MemoryCookieJar jar = new MemoryCookieJar();
        jar.put(URI.create("http://www.amdatu.org/foo"), "key=value; path=/; domain=amdatu.org");
        jar.put(URI.create("http://www.amdatu.org/foo"), "key=value; path=/foo; domain=amdatu.org");
        jar.put(URI.create("http://www.amdatu.org/bar"), "key=value; path=/bar; domain=www.amdatu.org");

        Set<Cookie> cookies;
        Iterator<Cookie> iter;
        Cookie c1, c2;

        cookies = jar.getCookies(URI.create("http://amdatu.org/"));
        assertNotNull(cookies);
        assertEquals(1, cookies.size());

        iter = cookies.iterator();
        c1 = iter.next();
        assertEquals("/", c1.getPath());
        assertEquals("amdatu.org", c1.getDomain());

        cookies = jar.getCookies(URI.create("http://amdatu.org/foo"));
        assertNotNull(cookies);
        assertEquals(2, cookies.size());

        iter = cookies.iterator();
        c1 = iter.next();
        assertEquals("/foo", c1.getPath());
        assertEquals("amdatu.org", c1.getDomain());

        c2 = iter.next();
        assertEquals("/", c2.getPath());
        assertEquals("amdatu.org", c2.getDomain());

        cookies = jar.getCookies(URI.create("http://www.amdatu.org/foo"));
        assertNotNull(cookies);
        assertEquals(2, cookies.size());

        iter = cookies.iterator();
        c1 = iter.next();
        assertEquals("/foo", c1.getPath());
        assertEquals("amdatu.org", c1.getDomain());

        c2 = iter.next();
        assertEquals("/", c2.getPath());
        assertEquals("amdatu.org", c2.getDomain());

        cookies = jar.getCookies(URI.create("http://www.amdatu.org/bar"));
        assertNotNull(cookies);
        assertEquals(2, cookies.size());

        iter = cookies.iterator();
        c1 = iter.next();
        assertEquals("/bar", c1.getPath());
        assertEquals("www.amdatu.org", c1.getDomain());

        c2 = iter.next();
        assertEquals("/", c2.getPath());
        assertEquals("amdatu.org", c2.getDomain());
    }

    @Test
    public void testParseCookiesWithValidExpirationDateOk() throws Exception {
        ZoneId gmt = ZoneId.of("GMT");
        ZoneId utc = ZoneId.of("UTC");

        Object[][] validDatePatterns =
            { { "Wed, 09 Jun 2021 10:18:14 GMT", ZonedDateTime.of(2021, 6, 9, 10, 18, 14, 0, gmt) },
                { "Wed, 09 Jun 2021 22:18:14 GMT", ZonedDateTime.of(2021, 6, 9, 22, 18, 14, 0, gmt) },
                { "Tue, 18 Oct 2011 07:42:42.123 GMT", ZonedDateTime.of(2011, 10, 18, 7, 42, 42, 0, gmt) },
                { "18 Oct 2011 07:42:42 GMT", ZonedDateTime.of(2011, 10, 18, 7, 42, 42, 0, gmt) },
                { "8 Oct 2011 7:42:42 GMT", ZonedDateTime.of(2011, 10, 8, 7, 42, 42, 0, gmt) },
                { "8 Oct 2011 7:2:42 GMT", ZonedDateTime.of(2011, 10, 8, 7, 2, 42, 0, gmt) },
                { "Oct 18 2011 07:42:42 GMT", ZonedDateTime.of(2011, 10, 18, 7, 42, 42, 0, gmt) },
                { "Tue Oct 18 2011 07:05:03 GMT+0000 (GMT)", ZonedDateTime.of(2011, 10, 18, 7, 5, 3, 0, gmt) },
                { "09 Jun 2021 10:18:14 GMT", ZonedDateTime.of(2021, 6, 9, 10, 18, 14, 0, gmt) },
                { "01 Jan 1970 00:00:00 GMT", ZonedDateTime.of(1970, 1, 1, 0, 0, 0, 0, gmt) },
                { "01 Jan 1601 00:00:00 GMT", ZonedDateTime.of(1601, 1, 1, 0, 0, 0, 0, gmt) },
                { "10 Feb 81 13:00:00 GMT", ZonedDateTime.of(1981, 2, 10, 13, 0, 0, 0, gmt) }, // implicit year
                { "Thu, 17-Apr-2014 02:12:29 GMT", ZonedDateTime.of(2014, 4, 17, 2, 12, 29, 0, gmt) }, // dashes
                { "Thu, 17-Apr-2014 02:12:29 UTC", ZonedDateTime.of(2014, 4, 17, 2, 12, 29, 0, utc) }, // dashes and UTC
            };

        for (Object[] vals : validDatePatterns) {
            String pattern = vals[0].toString();

            Cookie cookie = Cookie.parse("key=value; expires=" + pattern);
            assertEquals(pattern, ((ZonedDateTime) vals[1]).toLocalDateTime(), cookie.getExpirationTime());
        }
    }

    @Test
    public void testGetTimeBoundCookiesOk() throws Exception {
        MemoryCookieJar jar = new MemoryCookieJar();
        jar.put(URI.create("http://amdatu.org/"), "key=value; expires=12:00:00 31-dec-2099");

        Set<Cookie> cookies;
        Iterator<Cookie> iter;
        Cookie c1;

        cookies = jar.getCookies(URI.create("http://amdatu.org/"));
        assertNotNull(cookies);
        assertEquals(1, cookies.size());

        iter = cookies.iterator();
        c1 = iter.next();
        assertEquals("/", c1.getPath());
        assertEquals("amdatu.org", c1.getDomain());

        // Overwrite the cookie with a Max-Ago of 0: should result in the direction expiration of the cookie
        jar.put(URI.create("http://amdatu.org/"), "key=value; expires=12:00:00 31-dec-2099; max-age=0");

        cookies = jar.getCookies(URI.create("http://amdatu.org/"));
        assertNotNull(cookies);
        assertEquals(0, cookies.size());
    }
}
