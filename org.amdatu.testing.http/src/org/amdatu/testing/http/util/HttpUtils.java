/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.testing.http.util;

import static java.net.HttpURLConnection.HTTP_NOT_FOUND;
import static java.net.HttpURLConnection.HTTP_OK;
import static java.util.Arrays.stream;

import java.util.concurrent.TimeUnit;
import java.util.function.BiPredicate;
import java.util.function.Predicate;
import java.util.function.Supplier;

import com.github.kevinsawicki.http.HttpRequest;

/**
 * Provides utility methods to work with HTTP requests in unit/integration tests.
 */
public class HttpUtils {

    /**
     * @param expectedRC the expected response code, like 200 or 401. Should be a valid HTTP response code.
     * @param url the URL to check, cannot be <code>null</code>.
     * @return <code>true</code> if the given URL yielded the expected HTTP response code, <code>false</code> otherwise.
     */
    public static boolean checkEndpoint(int expectedRC, String url) {
        return HttpRequest.get(url).code() == expectedRC;
    }

    /**
     * @param url the URL to check, cannot be <code>null</code>.
     * @return <code>true</code> if the given URL yielded a HTTP 200 response.
     */
    public static boolean checkEndpointAvailable(String url) {
        return checkEndpoint(HTTP_OK, url);
    }

    /**
     * @param url the URL to check, cannot be <code>null</code>.
     * @return <code>true</code> if the given URL yielded a HTTP 404 response.
     */
    public static boolean checkEndpointNotFound(String url) {
        return checkEndpoint(HTTP_NOT_FOUND, url);
    }

    /**
     * @param expectedRC the expected response code, like 200 or 401. Should be a valid HTTP response code.
     * @param urls the URLs to check, at least one URL should be given.
     * @return <code>true</code> if all given URLs yield the expected HTTP response code, <code>false</code> otherwise.
     */
    public static boolean checkEndpoints(int expectedRC, String... urls) {
        return stream(urls).allMatch((url) -> checkEndpoint(expectedRC, url));
    }

    /**
     * @param urls the URLs to check, at least one URL should be given.
     * @return <code>true</code> if all given URLs yield a HTTP 200 response code, <code>false</code> otherwise.
     */
    public static boolean checkEndpointsAvailable(String... urls) {
        return checkEndpoints(HTTP_OK, urls);
    }

    public static boolean waitFor(int duration, TimeUnit timeUnit, BiPredicate<Integer, String> predicate, int expectedRC, String... urls) {
        return waitFor(duration, timeUnit, pURLs -> predicate.test(expectedRC, pURLs), urls);
    }

    public static boolean waitFor(int duration, TimeUnit timeUnit, Predicate<String> predicate, String... urls) {
        return waitFor(duration, timeUnit, () -> stream(urls).allMatch((url) -> predicate.test(url)));
    }

    public static boolean waitFor(int duration, TimeUnit timeUnit, Supplier<Boolean> supplier) {
        long until = System.currentTimeMillis() + timeUnit.toMillis(duration);
        while (System.currentTimeMillis() < until) {
            if (supplier.get()) {
                return true;
            }

            try {
                TimeUnit.MILLISECONDS.sleep(50L);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                return false;
            }
        }
        return false;
    }

    public static boolean waitForEndpointsAvailable(int duration, TimeUnit timeUnit, String... urls) {
        return waitFor(duration, timeUnit, HttpUtils::checkEndpointsAvailable, urls);
    }
}
