/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.testing.http.util;

import static java.util.regex.Pattern.compile;

import java.io.IOException;
import java.net.CookieHandler;
import java.net.URI;
import java.time.DateTimeException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

/**
 * Provides an in-memory cookie-handler that retains cookies in memory for the lifetime of the JVM.
 * <p>
 * This {@link CookieHandler} implementation handles cookies according to <a
 * href="https://www.ietf.org/rfc/rfc6265.txt">RFC 6265 (HTTP State Management Mechanism)</a>. It
 * does <b>not</b> rely on {@link java.net.HttpCookie} or the {@code javax.servlet.http.Cookie}
 * API as those implementations regard cookies from the server side in contrast to the client (user
 * agent) side. For example, they do not take the expiration of cookies into consideration.
 */
public class MemoryCookieJar extends CookieHandler {
    public static class Cookie {
        private static final String EXPIRES = "expires";
        private static final String MAX_AGE = "max-age";
        private static final String DOMAIN = "domain";
        private static final String PATH = "path";
        private static final String SECURE = "secure";
        private static final String HTTP_ONLY = "httponly";
        private static final String PERSISTENT = "persistent";
        private static final String HOST_ONLY = "hostonly";
        private static final String CREATION_TIME = "creationTime";

        private final String m_name;
        private final String m_value;
        private final Map<String, Object> m_properties;

        public Cookie(String cookieName, String cookieValue, Map<String, String> attrs, LocalDateTime creationTime) {
            if (cookieName == null || "".equals(cookieName)) {
                throw new IllegalArgumentException("Invalid name!");
            }

            m_name = cookieName;
            m_value = cookieValue;
            m_properties = new HashMap<>();

            m_properties.put(PERSISTENT, Boolean.FALSE);
            m_properties.put(DOMAIN, "");
            m_properties.put(PATH, null);
            m_properties.put(HOST_ONLY, Boolean.TRUE);
            m_properties.put(CREATION_TIME, creationTime);

            // Process the cookie attributes...
            for (Map.Entry<String, String> entry : attrs.entrySet()) {
                String key = entry.getKey().toLowerCase(Locale.US);
                String value = entry.getValue();
                switch (key) {
                    case EXPIRES:
                        LocalDateTime expDate = parseDate(value);
                        if (expDate != null) {
                            m_properties.put(EXPIRES, expDate);
                            m_properties.put(PERSISTENT, Boolean.TRUE);
                        }
                        break;
                    case MAX_AGE:
                        Long maxAge = parseNumber(value);
                        if (maxAge != null) {
                            if (maxAge.longValue() <= 0) {
                                expDate = LocalDateTime.ofEpochSecond(0L, 0, ZoneOffset.UTC);
                            } else {
                                expDate = LocalDateTime.now().plusSeconds(maxAge.longValue());
                            }
                            m_properties.put(EXPIRES, expDate);
                            m_properties.put(PERSISTENT, Boolean.TRUE);
                        }
                        break;
                    case DOMAIN:
                        if (value.startsWith(".")) {
                            value = value.substring(1);
                        }
                        if (!"".equals(value)) {
                            m_properties.put(DOMAIN, value.toLowerCase(Locale.US));
                            m_properties.put(HOST_ONLY, Boolean.FALSE);
                        }
                        break;
                    case PATH:
                        if (value == null || !value.startsWith(SLASH)) {
                            value = SLASH;
                        }
                        m_properties.put(PATH, value);
                        break;
                    case SECURE:
                        m_properties.put(SECURE, Boolean.TRUE);
                        break;
                    case HTTP_ONLY:
                        m_properties.put(HTTP_ONLY, Boolean.TRUE);
                        break;
                }
            }
        }

        public static Cookie parse(String str) {
            return parse(str, LocalDateTime.now());
        }

        public static Cookie parse(String str, LocalDateTime creationTime) {
            int idx = str.indexOf(';');

            String nameValue;
            String unparsedAttrs;
            if (idx >= 0) {
                nameValue = str.substring(0, idx);
                unparsedAttrs = str.substring(idx + 1, str.length());
            } else {
                nameValue = str;
                unparsedAttrs = "";
            }

            idx = nameValue.indexOf('=');
            if (idx < 0) {
                // Invalid cookie...
                return null;
            }

            String name = nameValue.substring(0, idx).trim();
            String value = nameValue.substring(idx + 1, nameValue.length()).trim();

            if ("".equals(name)) {
                // Invalid cookie...
                return null;
            }

            Map<String, String> attrs = new HashMap<>();

            for (String av : unparsedAttrs.split("\\s*;\\s*")) {
                idx = av.indexOf('=');
                if (idx < 0) {
                    attrs.put(av, null);
                } else {
                    String k = av.substring(0, idx).trim();
                    String v = av.substring(idx + 1, av.length()).trim();
                    // Overwrite any previous keys...
                    attrs.put(k, v);
                }
            }

            return new Cookie(name, value, attrs, creationTime);
        }

        private static boolean equals(Object obj1, Object obj2) {
            return (obj1 == null && obj2 == null) || (obj1 != null && obj1.equals(obj2));
        }

        private static int hashCode(Object obj) {
            return (obj == null) ? 0 : obj.hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }

            Cookie other = (Cookie) obj;
            if (!m_name.equals(other.m_name)) {
                return false;
            }
            if (!equals(getDomain(), other.getDomain())) {
                return false;
            }
            if (!equals(getPath(), other.getPath())) {
                return false;
            }
            return true;
        }

        public LocalDateTime getCreationTime() {
            return (LocalDateTime) m_properties.get(CREATION_TIME);
        }

        public String getDomain() {
            return (String) m_properties.get(DOMAIN);
        }

        public LocalDateTime getExpirationTime() {
            return (LocalDateTime) m_properties.get(EXPIRES);
        }

        public String getName() {
            return m_name;
        }

        public String getPath() {
            return (String) m_properties.get(PATH);
        }

        public String getValue() {
            return m_value;
        }

        @Override
        public int hashCode() {
            final int prime = 37;
            int result = 1;
            result = prime * result + hashCode(m_name);
            result = prime * result + hashCode(getDomain());
            result = prime * result + hashCode(getPath());
            return result;
        }

        public boolean isExpired() {
            LocalDateTime expDate = getExpirationTime();
            if (expDate == null) {
                return false;
            }
            return LocalDateTime.now().isAfter(expDate);
        }

        public boolean isHostOnly() {
            return Boolean.TRUE.equals(m_properties.get(HOST_ONLY));
        }

        public boolean isHttpOnly() {
            return Boolean.TRUE.equals(m_properties.get(HTTP_ONLY));
        }

        public boolean isPersistent() {
            return Boolean.TRUE.equals(m_properties.get(PERSISTENT));
        }

        public boolean isSecure() {
            return Boolean.TRUE.equals(m_properties.get(SECURE));
        }

        public boolean matchesDomain(String hostOrDomain) {
            hostOrDomain = canonicalize(hostOrDomain);

            String cookieDomain = getDomain();
            if (hostOrDomain.equals(cookieDomain)) {
                return true;
            }

            boolean isIPv4Address = hostOrDomain.matches("^\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}$");
            boolean isIPv6Address = hostOrDomain.matches("^\\[[0-9a-fA-F:]+\\]$");
            // Literal IPv4/6 address...?
            if (isIPv4Address || isIPv6Address) {
                return false;
            }

            return hostOrDomain.endsWith(cookieDomain) && (hostOrDomain.length() == cookieDomain.length()
                || (hostOrDomain.charAt(hostOrDomain.length() - cookieDomain.length() - 1) == '.'));
        }

        public boolean matchesPath(String requestPath) {
            String cookiePath = getPath();
            if (requestPath.equals(cookiePath)) {
                return true;
            }

            if (!requestPath.startsWith(cookiePath)) {
                return false;
            }

            if (cookiePath.endsWith(SLASH)) {
                return true;
            }

            return requestPath.charAt(cookiePath.length()) == '/';
        }

        protected void setCreationTime(LocalDateTime creationTime) {
            m_properties.put(CREATION_TIME, creationTime);
        }

        protected void setDomain(String domain) {
            if (!isHostOnly()) {
                throw new IllegalArgumentException("Can only update domain for host-only cookies!");
            }
            m_properties.put(DOMAIN, domain);
        }

        protected void setPath(String path) {
            if (m_properties.get(PATH) != null) {
                throw new IllegalArgumentException("Can only update path for cookies without path!");
            }
            m_properties.put(PATH, path);
        }
    }

    private static final Pattern RE_TIME = compile("^(\\d{1,2})[^\\d]*:(\\d{1,2})[^\\d]*:(\\d{1,2})[^\\d]*$");
    private static final Pattern RE_DOM = compile("^(\\d{1,2})[^\\d]*$");
    private static final Pattern RE_MONTH = compile("^(jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec)");
    private static final Pattern RE_YEAR = compile("^(\\d{2}|\\d{4})$");
    private static final String[] MONTHS = { "jan", "feb", "mar", "apr", "may", "jun", //
        "jul", "aug", "sep", "oct", "nov", "dec" };

    private static final String SLASH = "/";
    private static final String HDR_COOKIE = "Cookie";
    private static final String HDR_SET_COOKIE = "Set-Cookie";
    private static final String HTTP = "http";
    private static final String HTTPS = "https";

    private final Set<Cookie> m_jar;

    /**
     * Creates a new, empty, {@link MemoryCookieJar} instance.
     */
    public MemoryCookieJar() {
        // We use a plain HashSet to make use of mutable iterators!
        m_jar = new HashSet<>();
    }

    /**
     * @param host the hostname to canonicalize, cannot be <code>null</code>.
     * @return the canonicalized version of the given hostname, never <code>null</code>.
     */
    protected static String canonicalize(String host) {
        return host.toLowerCase(Locale.US); // canonicalize, this does not handle punycode!
    }

    /**
     * @param path the path to normalize and return the default value for, can be <code>null</code>.
     * @return the default path according to the rules specified in RFC 6265, never <code>null</code>.
     */
    protected static String defaultPath(String path) {
        if (path == null || !path.startsWith(SLASH)) {
            return SLASH;
        }
        path = path.substring(0, path.lastIndexOf(SLASH));
        if (path.length() == 0) {
            return SLASH;
        }
        return path;
    }

    /**
     * @param dateTime the date/time string to parse, can be <code>null</code> or empty.
     * @return the {@link LocalDateTime} representation of the given date/time string, or <code>null</code> if the parsing failed.
     */
    protected static LocalDateTime parseDate(String dateTime) {
        if (dateTime == null || "".equals(dateTime.trim())) {
            return null;
        }

        String[] parts = dateTime.split("[\\x09\\x20-\\x2F\\x3B-\\x40\\x5B-\\x60\\x7B-\\x7E]+");
        if (parts.length == 0) {
            return null;
        }

        int h = 0, m = 0, s = 0, dom = 0, mon = 0, year = 0;
        boolean found_time = false, found_dom = false, found_month = false, found_year = false;
        Matcher matcher;

        for (String part : parts) {
            part = part.trim();

            // 2.1 - parse time part...
            matcher = RE_TIME.matcher(part);
            if (!found_time && matcher.matches()) {
                found_time = true;
                h = Integer.valueOf(matcher.group(1));
                m = Integer.valueOf(matcher.group(2));
                s = Integer.valueOf(matcher.group(3));
                continue;
            }

            // 2.2 - parse day of month...
            matcher = RE_DOM.matcher(part);
            if (!found_dom && matcher.matches()) {
                found_dom = true;
                dom = Integer.valueOf(matcher.group(1));
                continue;
            }

            // 2.3 - parse month...
            matcher = RE_MONTH.matcher(part.toLowerCase());
            if (!found_month && matcher.matches()) {
                found_month = true;
                String match = matcher.group(1);
                mon = 1 + IntStream.range(0, MONTHS.length).filter(p -> MONTHS[p].equals(match)).findFirst().getAsInt();
                continue;
            }

            // 2.4 - parse year...
            matcher = RE_YEAR.matcher(part);
            if (!found_year && matcher.matches()) {
                found_year = true;
                year = Integer.valueOf(matcher.group(1));
                // correct year...
                if (year >= 70 && year < 100) {
                    year += 1900;
                }
                if (year >= 0 && year < 70) {
                    year += 2000;
                }
                continue;
            }
        }

        // validate the various date/time parts...
        if (!found_time || h > 23 || m > 59 || s > 59 || !found_dom || dom < 1 || dom > 31 || !found_month
            || !found_year || year < 1601) {
            return null;
        }

        try {
            return ZonedDateTime.of(year, mon, dom, h, m, s, 0, ZoneOffset.UTC).toLocalDateTime();
        } catch (DateTimeException e) {
            // Invalid date/time value...
            return null;
        }
    }

    /**
     * @param number the number string to parse, can be <code>null</code> or empty.
     * @return the {@link Long} representation of the given string, or <code>null</code> if parsing failed.
     */
    protected static Long parseNumber(String number) {
        if (number == null || "".equals(number.trim())) {
            return null;
        }
        try {
            return Long.valueOf(number);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    /**
     * Clears all stored cookies.
     */
    public void clear() {
        synchronized (m_jar) {
            m_jar.clear();
        }
    }

    /**
     * @return a shallow copy of the current set with stored cookies, never <code>null</code>.
     */
    public Set<Cookie> getCookies() {
        Set<Cookie> result;
        synchronized (m_jar) {
            result = new HashSet<>(m_jar);
        }
        return result;
    }

    @Override
    public Map<String, List<String>> get(URI uri, Map<String, List<String>> requestHeaders) throws IOException {
        StringBuilder sb = new StringBuilder();
        for (Cookie cookie : getCookies(uri)) {
            if (sb.length() > 0) {
                sb.append(';');
            }
            sb.append(cookie.getName());
            sb.append('=');
            sb.append(cookie.getValue());
        }

        Map<String, List<String>> result = new HashMap<String, List<String>>();
        if (sb.length() > 0) {
            result.put(HDR_COOKIE, Arrays.asList(sb.toString()));
        }
        return result;
    }

    @Override
    public void put(URI uri, Map<String, List<String>> responseHeaders) throws IOException {
        for (Map.Entry<String, List<String>> entry : responseHeaders.entrySet()) {
            String key = entry.getKey();
            if (!HDR_SET_COOKIE.equalsIgnoreCase(key)) {
                continue;
            }

            ListIterator<String> it = entry.getValue().listIterator(entry.getValue().size());
            while (it.hasPrevious()) {
                put(uri, it.previous());
            }
        }
    }

    /**
     * Returns the cookie string for a given URI as a single string value that can be directly used as HTTP header value.
     * 
     * @param uri the URI to return the cookies for, cannot be <code>null</code>.
     * @return a set with cookies that match the given URI, never <code>null</code>.
     */
    protected Set<Cookie> getCookies(URI uri) {
        String host = getCanonicalizedHost(uri);
        if (host == null) {
            return null;
        }

        String scheme = uri.getScheme();
        boolean secureProtocol = HTTPS.equalsIgnoreCase(scheme);
        boolean httpApi = HTTP.equalsIgnoreCase(scheme) || secureProtocol;

        return getMatchingCookies(host, uri.getPath(), secureProtocol, httpApi);
    }

    /**
     * Stores a string representation of a cookie for the given URI.
     * 
     * @param uri the URI that yielded the given cookie representation, cannot be <code>null</code>;
     * @param cookieStr the cookie string representation (according to RFC 6265) that should be stored.
     * @return <code>true</code> if the cookie was stored successfully, <code>false</code> otherwise.
     */
    protected boolean put(URI uri, String cookieStr) {
        String host = getCanonicalizedHost(uri);
        if (host == null) {
            return false;
        }

        Cookie cookie = Cookie.parse(cookieStr);
        if (cookie == null) {
            // Parsing failed...
            return false;
        }

        if (cookie.isHostOnly()) {
            cookie.setDomain(host);
        }
        if (!cookie.matchesDomain(host)) {
            // Cookie does *not* match the request-URI!
            return false;
        }

        if (cookie.getPath() == null) {
            // Make sure the cookie has a defined path!
            cookie.setPath(defaultPath(uri.getPath()));
        }

        String scheme = uri.getScheme();
        boolean httpApi = HTTP.equalsIgnoreCase(scheme) || HTTPS.equalsIgnoreCase(scheme);
        if (cookie.isHttpOnly() && !httpApi) {
            return false;
        }

        Cookie oldCookie = findCookie(cookie);
        if (oldCookie != null) {
            if (oldCookie.isHttpOnly() && !httpApi) {
                return false;
            }
            cookie.setCreationTime(oldCookie.getCreationTime());
        }

        return storeCookie(cookie);
    }

    private Cookie findCookie(Cookie cookie) {
        synchronized (m_jar) {
            Iterator<Cookie> cookieIter = m_jar.iterator();
            while (cookieIter.hasNext()) {
                Cookie candidate = cookieIter.next();
                // Purge expired cookies...
                if (candidate.isExpired()) {
                    cookieIter.remove();
                    continue;
                }

                if (candidate.equals(cookie)) {
                    return candidate;
                }
            }
            return null;
        }
    }

    private String getCanonicalizedHost(URI uri) {
        String host = uri.getHost();
        if (host == null || "".equals(host)) {
            return null;
        }
        return canonicalize(host);
    }

    private Set<Cookie> getMatchingCookies(String host, String path, boolean secureProtocol, boolean httpApi) {
        SortedSet<Cookie> result = new TreeSet<>((c1, c2) -> {
            int r = c2.getPath().compareTo(c1.getPath());
            if (r == 0) {
                r = c1.getCreationTime().compareTo(c2.getCreationTime());
            }
            return r;
        });

        synchronized (m_jar) {
            Iterator<Cookie> cookieIter = m_jar.iterator();
            while (cookieIter.hasNext()) {
                Cookie candidate = cookieIter.next();
                // Purge expired cookies...
                if (candidate.isExpired()) {
                    cookieIter.remove();
                    continue;
                }

                if (!candidate.matchesDomain(host) || !candidate.matchesPath(path)
                    || (secureProtocol && !candidate.isSecure()) || (!httpApi && candidate.isHttpOnly())) {
                    continue;
                }
                // Candidate matches
                result.add(candidate);
            }
            return result;
        }
    }

    private boolean storeCookie(Cookie cookie) {
        boolean added = false;

        synchronized (m_jar) {
            Iterator<Cookie> cookieIter = m_jar.iterator();
            while (cookieIter.hasNext()) {
                Cookie candidate = cookieIter.next();
                if (candidate.equals(cookie)) {
                    if (cookie.isExpired() || candidate.isExpired()) {
                        cookieIter.remove();
                    }
                    if (cookie.isExpired()) {
                        return false;
                    } else {
                        m_jar.add(cookie);
                        added = true;
                    }
                }
            }

            if (!added) {
                added = m_jar.add(cookie);
            }
        }

        return added;
    }
}
