/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.testing.http;

/**
 * Utility methods for creating the various configuration steps.
 */
public class HttpTestConfigurator {

    private HttpTestConfigurator() {
        // Nop
    }

    /**
     * Creates a new configuration step that waits until one or more HTTP endpoints are available
     * (= by default waits for them to return a response code of 200, "HTTP OK").
     * 
     * @param urls the URLs to wait for, cannot be empty.
     * @return a new {@link HttpEndpointsAvailableStep} step instance with the given URLs preconfigured.
     */
    public static HttpEndpointsAvailableStep createWaitForHttpEndpoints(String... urls) {
        return new HttpEndpointsAvailableStep().addURLs(urls);
    }

    /**
     * Creates a new configuration step that ensures that the JVM can make use of cookies during
     * the test case.
     * 
     * @return a new {@link HttpClientCookieJarStep} step instance.
     */
    public static HttpClientCookieJarStep useLocalCookieJar() {
        return new HttpClientCookieJarStep();
    }
}
