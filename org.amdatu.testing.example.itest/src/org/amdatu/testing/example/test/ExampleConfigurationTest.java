/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.testing.example.test;

import static org.amdatu.testing.configurator.TestConfigurator.cleanUp;
import static org.amdatu.testing.configurator.TestConfigurator.configure;
import static org.amdatu.testing.configurator.TestConfigurator.createConfiguration;
import static org.amdatu.testing.configurator.TestConfigurator.createServiceDependency;
import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.amdatu.testing.example.Example;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ExampleConfigurationTest {

	private volatile Example serviceToTest;

	@Before
	public void setup() throws InterruptedException {
		
		configure(this).add(createServiceDependency().setService(Example.class).setRequired(true))
				.add(createConfiguration("org.amdatu.testing.example").set("message", "Hi")).apply();
		
		TimeUnit.SECONDS.sleep(1);
	}

	@Test
	public void test() {
		String hello = serviceToTest.hello();
		assertEquals("Hi from org.amdatu.testing.example", hello);
	}

	@After
	public void after() {
		cleanUp(this);
	}
}
