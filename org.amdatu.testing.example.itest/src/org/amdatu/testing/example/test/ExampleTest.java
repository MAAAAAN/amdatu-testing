/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.testing.example.test;

import static org.amdatu.testing.configurator.TestConfigurator.*;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.After;
import org.amdatu.testing.example.Example;

public class ExampleTest {

	private volatile Example serviceToTest;

	@Before
	public void setup() {
		configure(this).add(createServiceDependency().setService(Example.class).setRequired(true)).apply();
	}

	@Test
	public void test() {
		String hello = serviceToTest.hello();
		assertEquals("Hello from org.amdatu.testing.example", hello);
	}

	@After
	public void after() {
		cleanUp(this);
	}
}