/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.testing.configurator.test;

import static org.amdatu.testing.configurator.TestConfigurator.configure;
import static org.amdatu.testing.configurator.TestConfigurator.*;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.amdatu.testing.configurator.ConfigurationStep;
import org.amdatu.testing.configurator.InjectionFailedException;
import org.junit.Test;

/**
 * If a setUp method fails with an InjectionFailureException, Amdatu testing should
 * clean up the steps taken so far. Otherwise other (subsequent) tests may fail because
 * of lingering dependencies and because the ThreadLocal is still set with the old test config.
 * 
 * Therefore, this test will break other tests if the correct cleanup procedure is not
 * called in {@see TestConfiguration}.
 */
public class CleanupAfterInjectionFailureTest {

    @Test
    public void testCleanUpIsCalledAfterInjectionFailure() throws InterruptedException {
        final CountDownLatch latch = new CountDownLatch(1);

        try {
            configure(new InnerTestService())
                .setTimeout(10, TimeUnit.MILLISECONDS)
                .add(new ConfigurationStep() {
                    @Override
                    public void cleanUp(Object testCase) {
                        latch.countDown();
                    }
    
                    @Override
                    public void apply(Object testCase) {
                        // Nop
                    }
                })
                .add(createServiceDependency().setService(NeverGetsInjected.class).setRequired(true))
                .add(createComponent().setImplementation(this).add(createConfigurationDependency().setPid("does.not.exist")))
                .apply();
        } catch (InjectionFailedException e) {
            assertTrue(e.getMessage().contains(NeverGetsInjected.class.getName()));
            assertTrue(e.getMessage().contains("does.not.exist"));
            assertTrue("Should have called cleanup after InjectionFailedException!", latch.await(2, TimeUnit.SECONDS));
            return;
        }

        fail("Should have thrown InjectionFailedeException!");
    }

    public static class InnerTestService {
        // Nop
    }

    public static interface NeverGetsInjected {
        // Nop
    }

}
