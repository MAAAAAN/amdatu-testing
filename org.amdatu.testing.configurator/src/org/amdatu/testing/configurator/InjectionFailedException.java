/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.testing.configurator;

/**
 * Exception thrown when a injection failed for some reason.
 */
public class InjectionFailedException extends TestConfigurationException {
    private static final long serialVersionUID = -8398032976997902228L;

    private final String m_componentDescription;

    public InjectionFailedException(String message, String componentDescription) {
        super(message);
        m_componentDescription = componentDescription;
    }

    public InjectionFailedException(String message, Throwable cause) {
        super(message, cause);
        m_componentDescription = "";
    }

    @Override
    public String getMessage() {
        StringBuilder sb = new StringBuilder(super.getMessage()).append("\n");
        return sb.append(m_componentDescription).append("\n").toString().trim();
    }
}
